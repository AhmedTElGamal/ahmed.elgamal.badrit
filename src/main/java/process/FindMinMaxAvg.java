package process;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class FindMinMaxAvg {

    public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {

        private Text userID = new Text();
        private IntWritable spendingPerVisit = new IntWritable();
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            String[] keyvalue = line.split("\t");
            userID.set(new Text(keyvalue[0]));
            spendingPerVisit.set(Integer.parseInt(keyvalue[2]));

            context.write(userID, spendingPerVisit);
        }
    }

    public static class Reduce extends Reducer<Text, IntWritable, Text, FloatWritable> {

        public void reduce(Text key, Iterable<IntWritable> values, Context context)
                throws IOException, InterruptedException {
            int sum = 0;
            int visitsCounter = 0;
            int min = Integer.MAX_VALUE;
            int max = Integer.MIN_VALUE;
            float avg;
            for (IntWritable val : values) {
                int currentValue = val.get();
                sum += currentValue;
                visitsCounter++;
                min = Math.min(min, currentValue);
                max = Math.max(max, currentValue);
            }
            avg = sum / visitsCounter;

            Text k1 = new Text("Max of " + "\t" + key.toString());
            context.write(k1, new FloatWritable(max) );

            Text k2 = new Text("Min of " + "\t" + key.toString() );
            context.write(k2, new FloatWritable(min));

            Text k3 = new Text("Avg of " + "\t"  +key.toString());
            context.write(k3, new FloatWritable(avg));


        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        Job job = new Job(conf, "findMinMax");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);


        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.waitForCompletion(true);
    }

}