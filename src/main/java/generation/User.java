package generation;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import static generation.DataGeneration.PRODUCT_COUNT;
import static generation.DataGeneration.RAND;

public class User {
    private int id, freq, no_of_products, no_of_visits;
    private boolean[] taken;
    private ArrayList<Integer> setOfInterestProducts, pickedProducts;

    public int getRandomQuantity(){
        return RAND.nextInt(200)+50;
    }

    private int getRandomProductNumber(boolean[] taken) {
        int productNumber = RAND.nextInt(PRODUCT_COUNT);
        return productNumber;
    }

    public int getRandomPrice(){
        return RAND.nextInt(195)+5;
    }

    private int getRandomProductQuantity() {
        return RAND.nextInt(9)+1;
    }

    public int getRandomDay(int frequency){
        int day = RAND.nextInt(120);
        try {
            while (day % frequency != 1) {
                day = RAND.nextInt(120);
            }
        }catch(Exception e){

        }
        return day;
    }

    private void print(int id, int visitNumber, int product, int quantity, int productPrice, BufferedWriter bw) throws IOException {
        String transaction = String.valueOf(id) + "\t" + String.valueOf(visitNumber) + "\t" + String.valueOf(product) + "\t" + String.valueOf(quantity) + "\t" + String.valueOf(productPrice)+ "\n";
        bw.write(transaction);
    }

    public User(int id, int freq, int no_of_products, int no_of_visits, BufferedWriter bw){
        this.id = id;
        this.freq = freq;
        this.no_of_products = no_of_products;
        this.no_of_visits = no_of_visits;

        taken = new boolean [30000 + 5];
        setOfInterestProducts = new ArrayList<Integer>();
        pickedProducts = new ArrayList<Integer>();

        for(int i=0 ; i<no_of_products ; i++){
            int productNo = getRandomProductNumber(taken);
            setOfInterestProducts.add(productNo);
            taken[productNo] = true;
        }

        for(int i=0 ; i<no_of_visits ; i++){
            int noOfPickedProducts = no_of_products/10;
            for(int j=0 ;j < noOfPickedProducts ; j++){
                pickedProducts = fillPickedProducts(noOfPickedProducts, setOfInterestProducts);
                int quantity = getRandomProductQuantity();
                try {
                    print(id, getRandomDay(freq),pickedProducts.get(j) , quantity, getRandomPrice(), bw);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private ArrayList<Integer> fillPickedProducts(int noOfPickedProducts, ArrayList<Integer> setOfInterestProducts){
        boolean[] taken = new boolean [30000];
        ArrayList<Integer> pickedProducts = new ArrayList<Integer>();
        for(int i=0 ; i<noOfPickedProducts ; i++){
            int productNumber = RAND.nextInt(30000);
            while(taken[productNumber]){
                productNumber = RAND.nextInt(30000);
            }
            pickedProducts.add(productNumber);
        }
        return pickedProducts;
    }


    public int getID(){
        return id;
    }

    public int getFreq(){
        return freq;
    }

    public int getNo_of_products(){
        return no_of_products;
    }

    public ArrayList<Integer> getSetOfInterestProducts(){return setOfInterestProducts; }

    public ArrayList<Integer> getPickedProducts(){return setOfInterestProducts; }
}
