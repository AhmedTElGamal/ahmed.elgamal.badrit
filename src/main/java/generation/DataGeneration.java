package generation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;


public class DataGeneration {
    public static int USER_COUNT = 10;
    public static final int PRODUCT_COUNT = 30000;
    public static Random RAND = new Random();
    private File file;
    private FileWriter fw;
    private BufferedWriter bw;


    public int getRandomFrequency(){
        return RAND.nextInt(10);
    }

    public int getRandomQuantity(){
        return RAND.nextInt(200)+50;
    }

    private int getRandomVisitsNumber() {
        return RAND.nextInt(4);
    }

    public void generate(String path) throws Exception {

        int freq, no_of_products, no_of_visits;
        file = new File(path);

        if (!file.exists()) {
            file.createNewFile();
        }

        fw = new FileWriter(file.getAbsoluteFile());
        bw = new BufferedWriter(fw);


        for (int i=0 ; i<USER_COUNT ; i++){
            freq = getRandomFrequency();
            no_of_products = getRandomQuantity();
            no_of_visits = getRandomVisitsNumber();
            User user = new User(i, freq, no_of_products, no_of_visits, bw);
        }
        bw.close();
        System.out.println("Data generation finished");
    }

}