package generation;

public class Product {
    private int price;
    private int id;

    public Product(int id, int price){
        this.price = price;
        this.id = id;
    }

    public int getPrice(){
        return price;
    }

    public int getID(){
        return id;
    }
}
