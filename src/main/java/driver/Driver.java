package driver;

import generation.DataGeneration;
import process.HDFSCommunication;

import java.io.IOException;
import java.net.URISyntaxException;


public class Driver {

    public static  void main(String[] args) throws Exception {

        DataGeneration generation = new DataGeneration();
        HDFSCommunication hdfs = new HDFSCommunication();
        final String fileName = "generatedData";

        generation.generate(fileName);
        //hdfs.copyFromLocal(fileName);


    }



}
